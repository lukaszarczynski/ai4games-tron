STDOUT.sync = true # DO NOT REMOVE

BOARD_HEIGHT = 20
BOARD_WIDTH = 30


class Directions < Hash
  def initialize
    self['RIGHT'] = lambda { |x| [x[0] + 1, x[1]] }
    self['LEFT'] = lambda { |x| [x[0] - 1, x[1]] }
    self['DOWN'] = lambda { |x| [x[0], x[1] + 1] }
    self['UP'] = lambda { |x| [x[0], x[1] - 1] }
  end

  def next_direction(current_direction)
    self.keys[(self.keys.index(current_direction) + 1) % 4]
  end

  def neighbors(position)
    _neighbors = []
    self.each_value { |direction| _neighbors << direction.call(position) }
    _neighbors
  end
end


class Player
  attr_reader :number
  attr_accessor :current_position, :track, :virtual_positions

  def initialize(player_number)
    @number = player_number
    @current_position = nil
    @virtual_positions = []
  end

  def update(position)
    @current_position = position
  end
end


class Board
  attr_reader :board
  def initialize
    @board = Hash.new(-1)
    (0...BOARD_WIDTH).each { |i| (0...BOARD_HEIGHT).each { |j| @board[[i, j]] = :free } }
    @directions = Directions.new
  end

  def free_neighbors(position)
    @directions.neighbors(position).select { |neighbor| @board[neighbor] == :free}
  end

  def opposite_neighbors?(position1, position2)
    (position1[0] == position2[0]) != (position1[1] == position2[1])
  end
end


class Game_state
  def initialize(number_of_players, my_number)
    @number_of_players, @my_number = number_of_players, my_number
    @players = []
    (0...@number_of_players).each { |i| @players.push(Player.new(i)) }
    @my_player = @players[@my_number]
    @board = Board.new
    @directions = Directions.new
  end

  def first_update
    @players.each do |player|
      # x0: starting X coordinate of lightcycle (or -1)
      # y0: starting Y coordinate of lightcycle (or -1)
      # x1: starting X coordinate of lightcycle (can be the same as X0 if you play before this player)
      # y1: starting Y coordinate of lightcycle (can be the same as Y0 if you play before this player)
      x0, y0, x1, y1 = gets.split(' ').collect { |x| x.to_i }
      STDERR.puts "#{x0} #{y0} #{x1} #{y1}"
      player.update([x1, y1])
      @board.board[[x1, y1]], @board.board[[x0, y0]] = player.number, player.number
    end
  end

  def update
    gets # these data were saved, ignore them
    t1 = Time.now
    @players.each do |player|
      x, y = gets.split(' ')[2..3].collect { |x| x.to_i }
      STDERR.puts "#{x} #{y}"
      player.update([x, y])
      @board.board[[x, y]] = player.number
      if x == -1
        @board.board.select { |_, field| field == player.number}.each_key do |directions|
          @board.board[directions] = :free
        end
      end
    end
    t2 = Time.now
    STDERR.puts "time update without gets: #{(t2-t1) * 1000}"
  end

  def move
    proposed_move = 'LEFT'
    alternative_move = @directions.next_direction(proposed_move)
    proposed_position = @directions[proposed_move].call(@my_player.current_position)
    while @board.board[proposed_position] != :free
      proposed_move = alternative_move
      alternative_move = @directions.next_direction(proposed_move)
      proposed_position = @directions[proposed_move].call(@my_player.current_position)
    end
    free_neighbors = @board.free_neighbors(@my_player.current_position)
    STDERR.puts 'Start flooding'
    t1 = Time.now
    free_neighbors.map! { |neighbor| [neighbor, count_simultaneously_flooded(neighbor)]}
    t2 = Time.now
    STDERR.puts "flood time: #{(t2-t1) * 1000}"
    STDERR.puts "free_neighbors: #{free_neighbors[0]}, #{free_neighbors[1]}, #{free_neighbors[2]}, #{free_neighbors[3]}"
    unless free_neighbors.uniq { |pair| pair[1]}.size == 1
      best_next_location = free_neighbors.max { |a, b| a[1] <=> b[1] }[0]
      neighbors_with_directions = {}
      @directions.each do |direction_name, direction|
        neighbors_with_directions[direction.call(@my_player.current_position)] = direction_name
      end
      proposed_move = neighbors_with_directions[best_next_location]
    end
    puts proposed_move
  end

  def count_simultaneously_flooded(start_position)
    board = @board.board.dup
    simultaneous_flood(start_position, board)
    board.each_value.select{ |field| field == @my_number}.size
  end

  def simultaneous_flood(position, board)
    my_original_position = @my_player.current_position
    @my_player.current_position = position
    @players.each { |player| player.virtual_positions = [player.current_position]}
    board[position] = @my_number
    should_continue = true
    while should_continue
      should_continue = false
      @players.each do |player|
        new_positions = []
        player.virtual_positions.each do |virtual_position|
          @board.free_neighbors(virtual_position).each do |neighbor|
            if board[neighbor] == :free
              board[neighbor] = player.number
              new_positions << neighbor
            end
          end
        end
        player.virtual_positions = new_positions
        should_continue = true unless new_positions.empty?
      end
    end
    @my_player.current_position = my_original_position
  end
end


# number_of_players: total number of players (2 to 4).
# my_number: your player number (0 to 3).
number_of_players, my_number = gets.split(' ').collect { |x| x.to_i }


t1 = Time.now


STDERR.puts "#{number_of_players} #{my_number}"
game_state = Game_state.new(number_of_players, my_number)
game_state.first_update
game_state.move


t2 = Time.now
STDERR.puts "time: #{(t2-t1) * 1000}"


# game loop
loop do
  t1 = Time.now

  game_state.update
  # Write an action using puts
  # To debug: STDERR.puts "Debug messages..."
  t2 = Time.now
  STDERR.puts "time update: #{(t2-t1) * 1000}"
  t2 = Time.now
  game_state.move
  t3 = Time.now
  STDERR.puts "time move: #{(t3-t2) * 1000}"
end